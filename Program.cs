﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
namespace LogInTeletinPOCU
{
    class Program
    {
        static void Main(string[] args)
        {
            // Open URL
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            driver.Url = "https://demoqa.com/automation-practice-form";
            driver.Manage().Window.Maximize();
            // Enter First Name and Enter Last Name
            driver.FindElement(By.Id("firstName")).SendKeys("Teletin");
            driver.FindElement(By.Id("lastName")).SendKeys("Alexandra-Ionela");


            //Enter email
            driver.FindElement(By.Id("userEmail")).SendKeys("alexandra_teletin@yahoo.com");

            // Select Gender (Female)
            driver.FindElement(By.XPath("//*[@id='genterWrapper']/div[2]/div[2]/label")).Click();
            driver.FindElement(By.Id("userNumber")).SendKeys("0734354656");
            driver.FindElement(By.Id("dateOfBirthInput")).Click();
            driver.FindElement(By.XPath("//*[@id='dateOfBirth']/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/div[1]/select")).SendKeys("August");
            driver.FindElement(By.XPath("//*[@id='dateOfBirth']/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/div[2]/select")).SendKeys("1998");
            driver.FindElement(By.XPath("//*[@id='dateOfBirth']/div[2]/div[2]/div/div/div[2]/div[2]/div[3]/div[7]")).Click();



            // Subjects
            //IWebElement subj=driver.FindElement(By.XPath("//*[@id='subjectsContainer']/div/div[1]"));
            // subj.Click();
            //driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/div[2]/div[1]/form/div[6]/div[2]/div/div")).SendKeys("Hello!");
            // subj.SendKeys("anaaremere");
            // System.Threading.Thread.Sleep(5000);
            //subj.SendKeys("Hello!");
            //driver.FindElement(By.Id("subjectsContainer")).SendKeys("Hello");



              //Select Hobbies
              driver.FindElement(By.XPath("//*[@id='hobbiesWrapper']/div[2]/div[2]/label")).Click();
            driver.FindElement(By.XPath("//*[@id='hobbiesWrapper']/div[2]/div[3]")).Click();




            //Uploud Picture
            IWebElement fileUploud = driver.FindElement(By.XPath("//*[@id='uploadPicture']"));
            fileUploud.SendKeys(@"C:\Users\Andra\Desktop\cat.jpg");


            //Current Address
            driver.FindElement(By.Id("currentAddress")).SendKeys("Bucuresti");


            // Close The Ad
            driver.FindElement(By.CssSelector("#close-fixedban > img")).Click();

            //State and city
            //State

            // driver.FindElement(By.CssSelector("#state > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)")).Click();
            var element = driver.FindElement(By.CssSelector("#state > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(element);
            actions.Perform();
            actions.Click();


            //driver.FindElement(By.XPath("//*[@id='state']/div/div[1]")).Click() ;
            //Actions actions = new Actions(driver);
            //actions.MoveToElement(element);
            //element.Click();
            //SelectElement StateSelect = new SelectElement(StateSelDrop);
            // StateSelect.SelectByText("NCR");

            //driver.FindElement(By.PartialLinkText("NCR")).Click();
            //System.Threading.Thread.Sleep(5000);
            //driver.FindElement(By.XPath("//*[@id='state']/div/div[1]")).SendKeys("NCR");

            //City
            //driver.FindElement(By.CssSelector("#city > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)")).Click();

            //Submit
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("submit")).Click();
            System.Threading.Thread.Sleep(5000);
            Assert.IsTrue(driver.FindElement(By.Id("example-modal-sizes-title-lg")).Displayed);
            driver.Quit();
        }



    }
}
